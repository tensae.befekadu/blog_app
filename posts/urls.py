from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('create', views.create, name='create'),
    path('<slug>', views.detail, name='detail'),
    path('<slug>/edit', views.update, name='update'),
    path('<slug>/delete', views.delete, name='delete'),
]
